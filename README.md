# JUnit-5

A quick overview of the changes that are coming with JUnit 5

## Context 

JUnit 5 can be divided into three sections.

* The core ````Platform```` (Mandatory)
* The ````Jupiter```` APIs to access all the new way of doing the testing 
* The ````Vintage```` APIs to access all the old way(JUnit-4) of doing the testing (Optional)
* The ````Extended```` way of customizing the APIs based on the project requirements (Optional)

### Notes

* JUnit 5 is not JUnit 4+1
* JUnit 5 tries to take full advantage of the new features from Java 8, especially lambda expressions.


## Dependencies and Setup

We need to add the following two dependencies in the pom.xml of the JAVA Maven project

	<dependency>
		<groupId>org.junit.jupiter</groupId>
		<artifactId>junit-jupiter-engine</artifactId>
		<version>5.4.0</version>
		<scope>test</scope>
	</dependency>

and,

	<dependency>
		<groupId>org.junit.jupiter</groupId>
		<artifactId>junit-jupiter-api</artifactId>
		<version>5.4.0</version>
		<scope>test</scope>
	</dependency>


### Setup : Project Structure

![junit setup](src/img/junit-setup.png)

## Assertions

	@Test
	void simpleMathExpressions() {
		assertTrue(Stream.of(1, 2, 3)
		  .stream()
		  .mapToInt(i -> i)
		  .sum() > 5, () -> "Sum should be greater than 5");
	}


It is possible to group assertions with assertAll() which will report any failed assertions within the group with a MultipleFailuresError:


	@Test
	void groupNumbers() {
		int[] numbers = {0, 1, 2, 3, 4};
		assertAll("numbers",
			() -> assertEquals(numbers[0], 1),
			() -> assertEquals(numbers[2], 2),
			() -> assertEquals(numbers[4], 3)
		);
	}


## Assumptions

Assumptions are used to run tests only if certain conditions are met. This is typically used for 3rd party integration that are required for the test to run properly. Assumptions understand lambda expressions.

````assumeTrue(), assumeFalse(), and assumingThat()```` methods can be used.

````assumeTrue() ````

	@Test
	void trueAssumption() {
		assumeTrue(5 > 1);
		assertEquals(5 + 2, 7);
	}

````assumeFalse()````

	@Test
	void falseAssumption() {
		assumeFalse(5 < 1);
		assertEquals(5 + 2, 7);
	}


## JUnit 4 - - to - -> JUnit 5


### @ Tags


Tags can be applied with the @ Tag annotation. These allow developers to group and filter tests like Categories of JUnit 4


	@Tag("Item Test")
	public class ItemDetailsTest {
		@Test
		@Tag("Method Item")
		void testItemSize() {
			assertEquals(2*5, 10);
		}
	}


### @ Disabled

Tests can now be disabled with the @ Disabled annotation. This test will not run. The @ Disabled annotation can be applied to either a test case or a test method.  Equivalent to @ Ignore from JUnit 4.


	@Test
	@Disabled
	void disabledTestCase() {
		assertTrue(false);
	}



















