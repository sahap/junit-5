package com.psaha.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class CalculatorTest {

	@Test
	public void testAdd() {
		Calculator oCalculator = new Calculator();
		int expected = 5;
		int actual = oCalculator.add(2, 3);
		assertEquals(expected, actual, "Return the Addition result");
	}

	@Test
	public void testSub() {
		Calculator oCalculator = new Calculator();
		int expected = -1;
		int actual = oCalculator.sub(2, 3);
		assertEquals(expected, actual, "Return the Subtraction result");
	}

	@Test
	public void testMul() {
		Calculator oCalculator = new Calculator();
		int expected = 6;
		int actual = oCalculator.mul(2, 3);
		assertEquals(expected, actual, "Return the Multiplication result");
	}

	@Test
	public void testArea() {
		Calculator oCalculator = new Calculator();
		double expected = 31.41;
		double actual = oCalculator.area(10);
		assertEquals(expected, actual, "Return the correct circle area");
	}

}
