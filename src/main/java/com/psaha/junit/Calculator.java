package com.psaha.junit;

public class Calculator {

	final double pi = Math.PI;

	// Addition
	public int add(int x, int y){
		return x + y;
	}

	// Subtraction 
	public int sub(int x, int y){
		return x - y;
	}

	// Multiplication
	public int mul(int x, int y){
		return x * y;
	}

	// Area Calculation
	public double area(int r){
		return 2 * pi * r;
	}

}
